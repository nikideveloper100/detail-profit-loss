{
    'name' : 'Detail Profit Loss',
    'version' : '1.0',
    'author' : 'Niks',
    'category' : 'Report',
    'description' : """
    
    """,
    'website': 'nothing',
    'images' : [],
    'depends' : ['base', 'account', 'account_budget'],
    'data': [
        "report_paperformat.xml",             
        "main_detail_profit_loss_view.xml",
        "wizard/account_detail_profit_loss_view.xml",
        "report_generalledger1.xml",
        
    ],
    'installable': True,
    'auto_install': False,
}