# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2005-2006 CamptoCamp
# Copyright (c) 2006-2010 OpenERP S.A
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
##############################################################################

import time
from openerp.osv import osv
from openerp.report import report_sxw
from openerp.addons.account.report.common_report_header import common_report_header


class general_ledger1(report_sxw.rml_parse, common_report_header):
    _name = 'report.account.general.ledger1'
    
    def set_context(self, objects, data, ids, report_type=None):
        print "calllllllllllllllllllllllllllll"
        new_ids = ids
        obj_move = self.pool.get('account.move.line')
        self.sortby = data['form'].get('sortby', 'sort_date')
        self.query = obj_move._query_get(self.cr, self.uid, obj='l', context=data['form'].get('used_context',{}))
        ctx2 = data['form'].get('used_context',{}).copy()
        self.init_balance = data['form'].get('initial_balance', True)
        if self.init_balance:
            ctx2.update({'initial_bal': True})
        self.init_query = obj_move._query_get(self.cr, self.uid, obj='l', context=ctx2)
        self.display_account = data['form']['display_account']
        self.target_move = data['form'].get('target_move', 'all')
        ctx = self.context.copy()
        ctx['fiscalyear'] = data['form']['fiscalyear_id']
        if data['form']['filter'] == 'filter_period':
            ctx['periods'] = data['form']['periods']
        elif data['form']['filter'] == 'filter_date':
            ctx['date_from'] = data['form']['date_from']
            ctx['date_to'] =  data['form']['date_to']
        ctx['state'] = data['form']['target_move']
        self.context.update(ctx)
        if (data['model'] == 'ir.ui.menu'):
            new_ids = [data['form']['chart_account_id']]
            objects = self.pool.get('account.account').browse(self.cr, self.uid, new_ids)
            
            
        fiscal_year = self._get_fiscalyear(data)
        sales_account=self.get_children_sales_accounts(objects)
        direct_account=self.get_children_direct_accounts(objects)
        admin_account=self.get_children_admin_accounts(objects)
        
        total_sales_plan=self._planned_amount_account(self.get_children_sales_accounts(objects))
        total_sales_actual=self._sum_credit_account(self.get_children_sales_accounts(objects))
        total_direct_plan=self._planned_amount_account(self.get_children_direct_accounts(objects))
        total_direct_actual=self._sum_credit_account(self.get_children_direct_accounts(objects))
        total_admin_plan=self._planned_amount_account(self.get_children_admin_accounts(objects))
        total_admin_actual=self._sum_credit_account(self.get_children_admin_accounts(objects))
        
        if total_sales_plan > total_sales_actual:
            total_per_sales=((total_sales_actual * 100 ) / total_sales_plan) - 100
        else:
            total_per_sales=((total_sales_actual - total_sales_plan) * 100 ) / total_sales_plan
        
        if total_direct_plan > total_direct_actual:
            total_per_direct=((total_direct_actual * 100 ) / total_direct_plan) - 100
        else:
            total_per_direct=((total_direct_actual - total_direct_plan) * 100 ) / total_direct_plan
        
        if total_admin_plan > total_admin_actual:
            total_per_admin=((total_admin_actual * 100 ) / total_admin_plan) - 100
        else:
            total_per_admin=((total_admin_actual - total_admin_plan) * 100 ) / total_admin_plan
            
            
        gross_plan=total_sales_plan - total_direct_plan
        gross_actual=total_sales_actual - total_direct_actual
        gross_var=gross_plan - gross_actual
        
        if gross_plan > gross_actual:
            gross_per=((gross_actual * 100 ) / gross_actual) - 100
        else:
            gross_per=((gross_actual - gross_plan) * 100 ) / gross_plan
            
        if total_sales_plan > total_direct_plan:
            gross_per_profit_plan=((total_direct_plan * 100 ) / total_sales_plan) - 100
        else:
            gross_per_profit_plan=((total_direct_plan - total_sales_plan) * 100 ) / total_sales_plan
            
        if total_sales_actual > total_direct_actual:
            gross_per_profit_actual=((total_direct_actual * 100 ) / total_sales_actual) - 100
        else:
            gross_per_profit_actual=((total_direct_actual - total_sales_actual) * 100 ) / total_sales_actual
            
        print "saaaaaaaaaaaaaaaa",sales_account
        print "saaaaaaaaaaaaaaaa",direct_account
        print "saaaaaaaaaaaaaaaa",admin_account
        
        print "saaaaaaaaaaaaaaaa",total_sales_plan
        print "saaaaaaaaaaaaaaaa",total_sales_actual
        print "saaaaaaaaaaaaaaaa",total_direct_plan
        print "saaaaaaaaaaaaaaaa",total_direct_actual
        
        sales_lines = {}
        for account in sales_account:
            m_line=[]
            plan_amount=self._planned_amount_account(account)
            actual_amount=self._sum_credit_account(account)
            variance_amount=plan_amount - actual_amount
            
            if plan_amount > actual_amount:
                per=((actual_amount * 100 ) / plan_amount) - 100
            else:
                per=((actual_amount - plan_amount) * 100 ) / plan_amount
            
            line={'plan_amount':plan_amount,'actual_amount':actual_amount,'variance_amount':variance_amount,'per':per}
            m_line.append(line)
            sales_lines[account.id]=m_line
            
            
        direct_lines = {}
        for account in direct_account:
            m_line=[]
            plan_amount=self._planned_amount_account(account)
            actual_amount=self._sum_credit_account(account)
            variance_amount=plan_amount - actual_amount
            
            if plan_amount > actual_amount:
                per=((actual_amount * 100 ) / plan_amount) - 100
            else:
                per=((actual_amount - plan_amount) * 100 ) / plan_amount
            
            line={'plan_amount':plan_amount,'actual_amount':actual_amount,'variance_amount':variance_amount,'per':per}
            m_line.append(line)
            direct_lines[account.id]=m_line

        admin_lines = {}
        for account in admin_account:
            m_line=[]
            plan_amount=self._planned_amount_account(account)
            actual_amount=self._sum_credit_account(account)
            variance_amount=plan_amount - actual_amount
            
            if plan_amount > actual_amount:
                per=((actual_amount * 100 ) / plan_amount) - 100
            else:
                per=((actual_amount - plan_amount) * 100 ) / plan_amount
            
            line={'plan_amount':plan_amount,'actual_amount':actual_amount,'variance_amount':variance_amount,'per':per}
            m_line.append(line)
            admin_lines[account.id]=m_line
        
        print "lllllllllllllllllllllllllllllll",sales_lines
        print "lllllllllllllllllllllllllllllll",direct_lines
        print "lllllllllllllllllllllllllllllll",admin_lines

#         stop_period = self.get_end_period(data)
#         open_balance=self._sum_balance_account(self.get_children_accounts(objects))
#         payment_balance=self._sum_credit_account(self.get_children_accounts(objects))
#         
        self.localcontext.update({
            'fiscal_year': fiscal_year,
            'sales_account': sales_account,
            'direct_account': direct_account,
            'admin_account': admin_account,
            'sales_lines': sales_lines,
            'direct_lines': direct_lines,
            'admin_lines': admin_lines,
            'total_sales_plan': total_sales_plan,
            'total_sales_actual': total_sales_actual,
            'total_direct_plan': total_direct_plan,
            'total_direct_actual': total_direct_actual,
            'total_per_sales': total_per_sales,
            'total_per_direct': total_per_direct,
            'total_per_admin': total_per_admin,
            'gross_plan': gross_plan,
            'gross_actual': gross_actual,
            'gross_var': gross_var,
            'gross_per': gross_per,
            'gross_per_profit_plan': gross_per_profit_plan,
            'gross_per_profit_actual': gross_per_profit_actual,
        })
            
        return super(general_ledger1, self).set_context(objects, data, new_ids, report_type=report_type)

    def __init__(self, cr, uid, name, context=None):
        if context is None:
            context = {}
        super(general_ledger1, self).__init__(cr, uid, name, context=context)
        self.query = ""
        self.query1 = ""
        self.tot_currency = 0.0
        self.period_sql = ""
        self.sold_accounts = {}
        self.sortby = 'sort_date'
        self.localcontext.update( {
            'time': time,
            'lines': self.lines,
            'sum_debit_account': self._sum_debit_account,
            'sum_credit_account': self._sum_credit_account,
            'sum_balance_account': self._sum_balance_account,
            'planned_amount_account': self._planned_amount_account,
            'sum_currency_amount_account': self._sum_currency_amount_account,
            'get_children_sales_accounts': self.get_children_sales_accounts,
            'get_children_direct_accounts': self.get_children_direct_accounts,
            'get_children_admin_accounts': self.get_children_admin_accounts,
            'get_children_direct_accounts_count': self.get_children_direct_accounts_count,
            'get_children_admin_accounts_count': self.get_children_admin_accounts_count,
            'get_fiscalyear': self._get_fiscalyear,
            'get_journal': self._get_journal,
            'get_account': self._get_account,
            'get_start_period': self.get_start_period,
            'get_end_period': self.get_end_period,
            'get_filter': self._get_filter,
            'get_sortby': self._get_sortby,
            'get_start_date':self._get_start_date,
            'get_end_date':self._get_end_date,
            'get_target_move': self._get_target_move,
        })
        self.context = context

    def _sum_currency_amount_account(self, account):
        self.cr.execute('SELECT sum(l.amount_currency) AS tot_currency \
                FROM account_move_line l \
                WHERE l.account_id = %s AND %s' %(account.id, self.query))
        sum_currency = self.cr.fetchone()[0] or 0.0
        if self.init_balance:
            self.cr.execute('SELECT sum(l.amount_currency) AS tot_currency \
                            FROM account_move_line l \
                            WHERE l.account_id = %s AND %s '%(account.id, self.init_query))
            sum_currency += self.cr.fetchone()[0] or 0.0
        return sum_currency

    def get_children_sales_accounts(self, account):
        print "AAAAAAAAAAA ",account
        res = []
        currency_obj = self.pool.get('res.currency')
        ids_acc = self.pool.get('account.account')._get_children_and_consol(self.cr, self.uid, account.id)
        currency = account.currency_id and account.currency_id or account.company_id.currency_id
        for child_account in self.pool.get('account.account').browse(self.cr, self.uid, ids_acc, context=self.context):
            sql = """
                SELECT count(id)
                FROM account_move_line AS l
                WHERE %s AND l.account_id = %%s
            """ % (self.query)
            self.cr.execute(sql, (child_account.id,))
            num_entry = self.cr.fetchone()[0] or 0
            sold_account = self._sum_balance_account(child_account)
            self.sold_accounts[child_account.id] = sold_account
            if self.display_account == 'movement':
                if child_account.type != 'view' and num_entry <> 0:
                    if child_account.detail_profit_loss=='Sales':
                        res.append(child_account)       
            elif self.display_account == 'not_zero':
                if child_account.type != 'view' and num_entry <> 0:
                    if not currency_obj.is_zero(self.cr, self.uid, currency, sold_account):
                        res.append(child_account)
            else:
                res.append(child_account)
        if not res:
            return [account]
        print "ssssssssssssssssssssssssss",res
        return res
    
    def get_children_direct_accounts(self, account):
        print "AAAAAAAAAAA ",account
        res = []
        currency_obj = self.pool.get('res.currency')
        ids_acc = self.pool.get('account.account')._get_children_and_consol(self.cr, self.uid, account.id)
        currency = account.currency_id and account.currency_id or account.company_id.currency_id
        for child_account in self.pool.get('account.account').browse(self.cr, self.uid, ids_acc, context=self.context):
            sql = """
                SELECT count(id)
                FROM account_move_line AS l
                WHERE %s AND l.account_id = %%s
            """ % (self.query)
            self.cr.execute(sql, (child_account.id,))
            num_entry = self.cr.fetchone()[0] or 0
            sold_account = self._sum_balance_account(child_account)
            self.sold_accounts[child_account.id] = sold_account
            if self.display_account == 'movement':
                if child_account.type != 'view' and num_entry <> 0:
                    if child_account.detail_profit_loss=='Direct':
                        res.append(child_account)       
            elif self.display_account == 'not_zero':
                if child_account.type != 'view' and num_entry <> 0:
                    if not currency_obj.is_zero(self.cr, self.uid, currency, sold_account):
                        res.append(child_account)
            else:
                res.append(child_account)
        if not res:
            return [account]
        print "ssssssssssssssssssssssssss",res
        return res
    
    def get_children_direct_accounts_count(self, account):
        print "AAAAAAAAAAA ",account
        res = []
        cnt=0
        currency_obj = self.pool.get('res.currency')
        ids_acc = self.pool.get('account.account')._get_children_and_consol(self.cr, self.uid, account.id)
        currency = account.currency_id and account.currency_id or account.company_id.currency_id
        for child_account in self.pool.get('account.account').browse(self.cr, self.uid, ids_acc, context=self.context):
            sql = """
                SELECT count(id)
                FROM account_move_line AS l
                WHERE %s AND l.account_id = %%s
            """ % (self.query)
            self.cr.execute(sql, (child_account.id,))
            num_entry = self.cr.fetchone()[0] or 0
            sold_account = self._sum_balance_account(child_account)
            self.sold_accounts[child_account.id] = sold_account
            if self.display_account == 'movement':
                if child_account.type != 'view' and num_entry <> 0:
                    if child_account.detail_profit_loss=='Direct':
                        res.append(child_account)
                        cnt+=1       
            elif self.display_account == 'not_zero':
                if child_account.type != 'view' and num_entry <> 0:
                    if not currency_obj.is_zero(self.cr, self.uid, currency, sold_account):
                        res.append(child_account)
            else:
                res.append(child_account)
        if not res:
            return [account]
        print "ssssssssssssssssssssssssss",res
        return cnt
    

    def get_children_admin_accounts(self, account):
        print "AAAAAAAAAAA ",account
        res = []
        currency_obj = self.pool.get('res.currency')
        ids_acc = self.pool.get('account.account')._get_children_and_consol(self.cr, self.uid, account.id)
        currency = account.currency_id and account.currency_id or account.company_id.currency_id
        for child_account in self.pool.get('account.account').browse(self.cr, self.uid, ids_acc, context=self.context):
            sql = """
                SELECT count(id)
                FROM account_move_line AS l
                WHERE %s AND l.account_id = %%s
            """ % (self.query)
            self.cr.execute(sql, (child_account.id,))
            num_entry = self.cr.fetchone()[0] or 0
            sold_account = self._sum_balance_account(child_account)
            self.sold_accounts[child_account.id] = sold_account
            if self.display_account == 'movement':
                if child_account.type != 'view' and num_entry <> 0:
                    if child_account.detail_profit_loss=='Administrative':
                        res.append(child_account)       
            elif self.display_account == 'not_zero':
                if child_account.type != 'view' and num_entry <> 0:
                    if not currency_obj.is_zero(self.cr, self.uid, currency, sold_account):
                        res.append(child_account)
            else:
                res.append(child_account)
        if not res:
            return [account]
        print "ssssssssssssssssssssssssss",res
        return res
    
    def get_children_admin_accounts_count(self, account):
        print "AAAAAAAAAAA ",account
        cnt=0
        res = []
        currency_obj = self.pool.get('res.currency')
        ids_acc = self.pool.get('account.account')._get_children_and_consol(self.cr, self.uid, account.id)
        currency = account.currency_id and account.currency_id or account.company_id.currency_id
        for child_account in self.pool.get('account.account').browse(self.cr, self.uid, ids_acc, context=self.context):
            sql = """
                SELECT count(id)
                FROM account_move_line AS l
                WHERE %s AND l.account_id = %%s
            """ % (self.query)
            self.cr.execute(sql, (child_account.id,))
            num_entry = self.cr.fetchone()[0] or 0
            sold_account = self._sum_balance_account(child_account)
            self.sold_accounts[child_account.id] = sold_account
            if self.display_account == 'movement':
                if child_account.type != 'view' and num_entry <> 0:
                    if child_account.detail_profit_loss=='Administrative':
                        res.append(child_account)
                        cnt+=1       
            elif self.display_account == 'not_zero':
                if child_account.type != 'view' and num_entry <> 0:
                    if not currency_obj.is_zero(self.cr, self.uid, currency, sold_account):
                        res.append(child_account)
            else:
                res.append(child_account)
        if not res:
            return [account]
        print "ssssssssssssssssssssssssss",res
        return cnt

    def lines(self, account):
        """ Return all the account_move_line of account with their account code counterparts """
        move_state = ['draft','posted']
        if self.target_move == 'posted':
            move_state = ['posted', '']
        # First compute all counterpart strings for every move_id where this account appear.
        # Currently, the counterpart info is used only in landscape mode
        sql = """
            SELECT m1.move_id,
                array_to_string(ARRAY(SELECT DISTINCT a.code
                                          FROM account_move_line m2
                                          LEFT JOIN account_account a ON (m2.account_id=a.id)
                                          WHERE m2.move_id = m1.move_id
                                          AND m2.account_id<>%%s), ', ') AS counterpart
                FROM (SELECT move_id
                        FROM account_move_line l
                        LEFT JOIN account_move am ON (am.id = l.move_id)
                        WHERE am.state IN %s and %s AND l.account_id = %%s GROUP BY move_id) m1
        """% (tuple(move_state), self.query)
        self.cr.execute(sql, (account.id, account.id))
        counterpart_res = self.cr.dictfetchall()
        counterpart_accounts = {}
        for i in counterpart_res:
            counterpart_accounts[i['move_id']] = i['counterpart']
        del counterpart_res

    # Then select all account_move_line of this account
        if self.sortby == 'sort_journal_partner':
            sql_sort='j.code, p.name, l.move_id'
        else:
            sql_sort='l.date, l.move_id'
        sql = """
            SELECT l.id AS lid, l.date AS ldate, j.code AS lcode, l.currency_id,l.amount_currency,l.ref AS lref, l.name AS lname, COALESCE(l.debit,0) AS debit, COALESCE(l.credit,0) AS credit, l.period_id AS lperiod_id, l.partner_id AS lpartner_id,
            m.name AS move_name, m.id AS mmove_id,per.code as period_code,
            c.symbol AS currency_code,
            i.id AS invoice_id, i.type AS invoice_type, i.number AS invoice_number,
            p.name AS partner_name
            FROM account_move_line l
            JOIN account_move m on (l.move_id=m.id)
            LEFT JOIN res_currency c on (l.currency_id=c.id)
            LEFT JOIN res_partner p on (l.partner_id=p.id)
            LEFT JOIN account_invoice i on (m.id =i.move_id)
            LEFT JOIN account_period per on (per.id=l.period_id)
            JOIN account_journal j on (l.journal_id=j.id)
            WHERE %s AND m.state IN %s AND l.account_id = %%s ORDER by %s
        """ %(self.query, tuple(move_state), sql_sort)
        self.cr.execute(sql, (account.id,))
        res_lines = self.cr.dictfetchall()
        res_init = []
        if res_lines and self.init_balance:
            #FIXME: replace the label of lname with a string translatable
            sql = """
                SELECT 0 AS lid, '' AS ldate, '' AS lcode, COALESCE(SUM(l.amount_currency),0.0) AS amount_currency, '' AS lref, 'Initial Balance' AS lname, COALESCE(SUM(l.debit),0.0) AS debit, COALESCE(SUM(l.credit),0.0) AS credit, '' AS lperiod_id, '' AS lpartner_id,
                '' AS move_name, '' AS mmove_id, '' AS period_code,
                '' AS currency_code,
                NULL AS currency_id,
                '' AS invoice_id, '' AS invoice_type, '' AS invoice_number,
                '' AS partner_name
                FROM account_move_line l
                LEFT JOIN account_move m on (l.move_id=m.id)
                LEFT JOIN res_currency c on (l.currency_id=c.id)
                LEFT JOIN res_partner p on (l.partner_id=p.id)
                LEFT JOIN account_invoice i on (m.id =i.move_id)
                JOIN account_journal j on (l.journal_id=j.id)
                WHERE %s AND m.state IN %s AND l.account_id = %%s
            """ %(self.init_query, tuple(move_state))
            self.cr.execute(sql, (account.id,))
            res_init = self.cr.dictfetchall()
        res = res_init + res_lines
        account_sum = 0.0
        for l in res:
            l['move'] = l['move_name'] != '/' and l['move_name'] or ('*'+str(l['mmove_id']))
            l['partner'] = l['partner_name'] or ''
            account_sum += l['debit'] - l['credit']
            l['progress'] = account_sum
            l['line_corresp'] = l['mmove_id'] == '' and ' ' or counterpart_accounts[l['mmove_id']].replace(', ',',')
            # Modification of amount Currency
            if l['credit'] > 0:
                if l['amount_currency'] != None:
                    l['amount_currency'] = abs(l['amount_currency']) * -1
            if l['amount_currency'] != None:
                self.tot_currency = self.tot_currency + l['amount_currency']
        return res

    def _sum_debit_account(self, account):
        if account.type == 'view':
            return account.debit
        move_state = ['draft','posted']
        if self.target_move == 'posted':
            move_state = ['posted','']
        self.cr.execute('SELECT sum(debit) \
                FROM account_move_line l \
                JOIN account_move am ON (am.id = l.move_id) \
                WHERE (l.account_id = %s) \
                AND (am.state IN %s) \
                AND '+ self.query +' '
                ,(account.id, tuple(move_state)))
        sum_debit = self.cr.fetchone()[0] or 0.0
        if self.init_balance:
            self.cr.execute('SELECT sum(debit) \
                    FROM account_move_line l \
                    JOIN account_move am ON (am.id = l.move_id) \
                    WHERE (l.account_id = %s) \
                    AND (am.state IN %s) \
                    AND '+ self.init_query +' '
                    ,(account.id, tuple(move_state)))
            # Add initial balance to the result
            sum_debit += self.cr.fetchone()[0] or 0.0
        return sum_debit

    def _sum_credit_account(self, account1):
        print "ddddddddddddddddddddd",account1
        sum_credit=0.0
        for account in account1:
            if account.type == 'view':
                return account.credit
            move_state = ['draft','posted']
            if self.target_move == 'posted':
                move_state = ['posted','']
            self.cr.execute('SELECT sum(credit) \
                    FROM account_move_line l \
                    JOIN account_move am ON (am.id = l.move_id) \
                    WHERE (l.account_id = %s) \
                    AND (am.state IN %s) \
                    AND '+ self.query +' '
                    ,(account.id, tuple(move_state)))
            sum_credit += self.cr.fetchone()[0] or 0.0
            if self.init_balance:
                self.cr.execute('SELECT sum(credit) \
                        FROM account_move_line l \
                        JOIN account_move am ON (am.id = l.move_id) \
                        WHERE (l.account_id = %s) \
                        AND (am.state IN %s) \
                        AND '+ self.init_query +' '
                        ,(account.id, tuple(move_state)))
                # Add initial balance to the result
                sum_credit += self.cr.fetchone()[0] or 0.0
        return sum_credit
    
    def _planned_amount_account(self,account1):
        print "ddddddddddddddddddddd",account1
        sum_balance=0.0
        for account in account1:
            self.cr.execute('SELECT sum(planned_amount) as planned_amount \
                    FROM crossovered_budget_lines l \
                    JOIN account_account am ON (am.id = l.account_id) \
                    WHERE (l.account_id ='+ str(account.id) +')')
            
            sum_balance += self.cr.fetchone()[0] or 0.0
        return sum_balance        

    def _sum_balance_account(self, account):
        if account.type == 'view':
            return account.balance
        move_state = ['draft','posted']
        if self.target_move == 'posted':
            move_state = ['posted','']
        self.cr.execute('SELECT (sum(debit) - sum(credit)) as tot_balance \
                FROM account_move_line l \
                JOIN account_move am ON (am.id = l.move_id) \
                WHERE (l.account_id = %s) \
                AND (am.state IN %s) \
                AND '+ self.query +' '
                ,(account.id, tuple(move_state)))
        sum_balance = self.cr.fetchone()[0] or 0.0
        if self.init_balance:
            self.cr.execute('SELECT (sum(debit) - sum(credit)) as tot_balance \
                    FROM account_move_line l \
                    JOIN account_move am ON (am.id = l.move_id) \
                    WHERE (l.account_id = %s) \
                    AND (am.state IN %s) \
                    AND '+ self.init_query +' '
                    ,(account.id, tuple(move_state)))
            # Add initial balance to the result
            sum_balance += self.cr.fetchone()[0] or 0.0
        return sum_balance

    def _get_account(self, data):
        if data['model'] == 'account.account':
            return self.pool.get('account.account').browse(self.cr, self.uid, data['form']['id']).company_id.name
        return super(general_ledger1 ,self)._get_account(data)

    def _get_sortby(self, data):
        if self.sortby == 'sort_date':
            return self._translate('Date')
        elif self.sortby == 'sort_journal_partner':
            return self._translate('Journal & Partner')
        return self._translate('Date')


class report_generalledger1(osv.AbstractModel):
    _name = 'report.detail_profit_loss.report_generalledger1'
    _inherit = 'report.abstract_report'
    _template = 'detail_profit_loss.report_generalledger1'
    _wrapped_report_class = general_ledger1

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
