# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 Noviat nv/sa (www.noviat.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlwt
from datetime import datetime
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell
from openerp.addons.detail_profit_loss.report.account_general_ledger1 import general_ledger1
from openerp.tools.translate import _
# import logging
# _logger = logging.getLogger(__name__)

_column_sizes = [
    ('no', 5),
    ('account', 40),
    ('plan', 20),
    ('actual', 20),
    ('variance', 20),
    ('per', 10),
]


class general_ledger_xls2(report_xls):
    column_sizes = [x[1] for x in _column_sizes]

    def generate_xls_report(self, _p, _xs, data, objects, wb):
        
        print "nameeeeeeeeeeeeee",objects
        print "fffffffffffffffffffff",_p.company.name
        print "cureeeeeeeeeee",_p.company.currency_id.symbol
        
        ws = wb.add_sheet("Detail Profit Loss")
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0
 
        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']
 
        # cf. account_report_general_ledger.mako
        initial_balance_text = {'initial_balance': _('Computed'),
                                'opening_balance': _('Opening Entries'),
                                False: _('No')}
 
        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        report_name = _p.company.name
        c_specs = [
            ('report_name', 6, 0, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        cell_format = _xs['bold'] + _xs['fill_blue'] + _xs['borders_all']
        cell_style = xlwt.easyxf(cell_format)
        cell_style_center = xlwt.easyxf(cell_format + _xs['center'])
        cell_style_left = xlwt.easyxf(cell_format + _xs['left'])
        cell_style_right = xlwt.easyxf(cell_format + _xs['right'])
        c_hdr_cell_style_decimal = xlwt.easyxf(cell_format + _xs['right'],num_format_str=report_xls.decimal_format)

        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
        
        
        year = 'Detail Profit And Loss Report for ' + _p.fiscal_year
        c_specs = [
               ('year', 6, 0, 'text', year),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
         
        # write empty row to define column sizes
        c_sizes = self.column_sizes
        c_specs = [('empty%s' % i, 1, c_sizes[i], 'text', None)
                   for i in range(0, len(c_sizes))]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, set_column_size=True)

        c_specs = [
               ('blank', 6, 0, 'text', ''),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
        
        c_specs = [
               ('no', 1, 0, 'text', 'A', None, cell_style_left),
               ('account', 1, 0, 'text', 'Sales', None, cell_style_left),
               ('plan_amount', 1, 0, 'text', 'Planned Amount', None, cell_style_right),
               ('actual_amount', 1, 0, 'text', 'Actual Amount', None, cell_style_right),
               ('variance', 1, 0, 'text', 'Variance', None, cell_style_right),
               ('per', 1, 0, 'text', '%', None, cell_style_right),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
 
        cnt=0
        row_start = row_pos
        for account in _p.sales_account:
            cnt=cnt+1
            print "aaaaaaaaaaaaaaaa",account
            
            for line in _p['sales_lines'][account.id]:
                print "lllllllllll",line.get('plan_amount')

                c_specs = [
                       ('no', 1, 0, 'number', cnt, None, cell_style_right),
                       ('account', 1, 0, 'text', ' - '.join([account.code, account.name]), None, cell_style_left),
                       ('plan_amount', 1, 0, 'number', line.get('plan_amount') or '', None, c_hdr_cell_style_decimal),
                       ('actual_amount', 1, 0, 'number', line.get('actual_amount') or '', None, c_hdr_cell_style_decimal),
                       ('variance', 1, 0, 'number', line.get('variance_amount') or '', None, c_hdr_cell_style_decimal),
                       ('per', 1, 0, 'number', line.get('per') or '', None, c_hdr_cell_style_decimal),
               ]
                
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_center)

            
        plan_start = rowcol_to_cell(row_start, 2)
        plan_end = rowcol_to_cell(row_pos - 1, 2)
        plan_formula = 'SUM(' + plan_start + ':' + plan_end + ')'
        
        actual_start = rowcol_to_cell(row_start, 3)
        actual_end = rowcol_to_cell(row_pos - 1, 3)
        actual_formula = 'SUM(' + actual_start + ':' + actual_end + ')'

        variance_start = rowcol_to_cell(row_start, 4)
        variance_end = rowcol_to_cell(row_pos - 1, 4)
        variance_formula = 'SUM(' + variance_start + ':' + variance_end + ')'

        c_specs = [
               ('account', 2, 0, 'text', 'TOTAL REVENUE', None, cell_style_right),
               ('plan_amount', 1, 0, 'number',None, plan_formula , c_hdr_cell_style_decimal),
               ('actual_amount', 1, 0, 'number', None, actual_formula , c_hdr_cell_style_decimal),
               ('variance', 1, 0, 'number',  None, variance_formula , c_hdr_cell_style_decimal),
               ('per', 1, 0, 'number', _p.total_per_sales or 0, None, c_hdr_cell_style_decimal),
        ]
                
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
        

        #Direct Expenses
        c_specs = [
               ('no', 1, 0, 'text', 'B', None, cell_style_left),
               ('account', 5, 0, 'text', 'Direct Expenses', None, cell_style_left),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
        
        row_start = row_pos
        for account in _p.direct_account:
            cnt=cnt+1
            print "aaaaaaaaaaaaaaaa",account
            
            for line in _p['direct_lines'][account.id]:
                print "lllllllllll",line.get('plan_amount')

                c_specs = [
                       ('no', 1, 0, 'number', cnt, None, cell_style_right),
                       ('account', 1, 0, 'text', ' - '.join([account.code, account.name]), None, cell_style_left),
                       ('plan_amount', 1, 0, 'number', line.get('plan_amount') or '', None, c_hdr_cell_style_decimal),
                       ('actual_amount', 1, 0, 'number', line.get('actual_amount') or '', None, c_hdr_cell_style_decimal),
                       ('variance', 1, 0, 'number', line.get('variance_amount') or '', None, c_hdr_cell_style_decimal),
                       ('per', 1, 0, 'number', line.get('per') or '', None, c_hdr_cell_style_decimal),
               ]
                
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_center)

            
        plan_start = rowcol_to_cell(row_start, 2)
        plan_end = rowcol_to_cell(row_pos - 1, 2)
        plan_formula = 'SUM(' + plan_start + ':' + plan_end + ')'
        
        actual_start = rowcol_to_cell(row_start, 3)
        actual_end = rowcol_to_cell(row_pos - 1, 3)
        actual_formula = 'SUM(' + actual_start + ':' + actual_end + ')'

        variance_start = rowcol_to_cell(row_start, 4)
        variance_end = rowcol_to_cell(row_pos - 1, 4)
        variance_formula = 'SUM(' + variance_start + ':' + variance_end + ')'

        c_specs = [
               ('account', 2, 0, 'text', 'TOTAL', None, cell_style_right),
               ('plan_amount', 1, 0, 'number',None, plan_formula , c_hdr_cell_style_decimal),
               ('actual_amount', 1, 0, 'number', None, actual_formula , c_hdr_cell_style_decimal),
               ('variance', 1, 0, 'number',  None, variance_formula , c_hdr_cell_style_decimal),
               ('per', 1, 0, 'number', _p.total_per_direct or 0, None, c_hdr_cell_style_decimal),
        ]
                
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
        
        
        c_specs = [
               ('blank', 6, 0, 'text', ''),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
        
        #Gross Expenses        
        c_specs = [
               ('no', 1, 0, 'text', 'C', None, cell_style_left),
               ('account', 1, 0, 'text', 'Gross Profit', None, cell_style_left),
               ('plan_amount', 1, 0, 'number', _p.gross_plan or '', None, c_hdr_cell_style_decimal),
               ('actual_amount', 1, 0, 'number', _p.gross_actual or '', None, c_hdr_cell_style_decimal),
               ('variance', 1, 0, 'number', _p.gross_var or '', None, c_hdr_cell_style_decimal),
               ('per', 1, 0, 'number',  _p.gross_per or '', None, c_hdr_cell_style_decimal),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
        
        c_specs = [
               ('no', 1, 0, 'text', '', None, cell_style_left),
               ('account', 1, 0, 'text', 'Gross Profit Margin', None, cell_style_left),
               ('plan_amount', 1, 0, 'number', _p.gross_per_profit_plan or '', None, c_hdr_cell_style_decimal),
               ('actual_amount', 1, 0, 'number', _p.gross_per_profit_actual or '', None, c_hdr_cell_style_decimal),
               ('variance', 1, 0, 'text', '', None, cell_style_left),
               ('per', 1, 0, 'text',  '', None, cell_style_left),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
        
        c_specs = [
               ('blank', 6, 0, 'text', ''),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)

        
        #Admin Expenses        
        c_specs = [
               ('no', 1, 0, 'text', 'D', None, cell_style_left),
               ('account', 5, 0, 'text', 'Administrative Expenses', None, cell_style_left),
        ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style_center)
        
        row_start = row_pos
        for account in _p.admin_account:
            cnt=cnt+1
            print "aaaaaaaaaaaaaaaa",account
            
            for line in _p['admin_lines'][account.id]:
                print "lllllllllll",line.get('plan_amount')

                c_specs = [
                       ('no', 1, 0, 'number', cnt, None, cell_style_right),
                       ('account', 1, 0, 'text', ' - '.join([account.code, account.name]), None, cell_style_left),
                       ('plan_amount', 1, 0, 'number', line.get('plan_amount') or '', None, c_hdr_cell_style_decimal),
                       ('actual_amount', 1, 0, 'number', line.get('actual_amount') or '', None, c_hdr_cell_style_decimal),
                       ('variance', 1, 0, 'number', line.get('variance_amount') or '', None, c_hdr_cell_style_decimal),
                       ('per', 1, 0, 'number', line.get('per') or '', None, c_hdr_cell_style_decimal),
               ]
                
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_center)
            
        plan_start = rowcol_to_cell(row_start, 2)
        plan_end = rowcol_to_cell(row_pos - 1, 2)
        plan_formula = 'SUM(' + plan_start + ':' + plan_end + ')'
        
        actual_start = rowcol_to_cell(row_start, 3)
        actual_end = rowcol_to_cell(row_pos - 1, 3)
        actual_formula = 'SUM(' + actual_start + ':' + actual_end + ')'

        variance_start = rowcol_to_cell(row_start, 4)
        variance_end = rowcol_to_cell(row_pos - 1, 4)
        variance_formula = 'SUM(' + variance_start + ':' + variance_end + ')'

        c_specs = [
               ('account', 2, 0, 'text', 'TOTAL', None, cell_style_right),
               ('plan_amount', 1, 0, 'number',None, plan_formula , c_hdr_cell_style_decimal),
               ('actual_amount', 1, 0, 'number', None, actual_formula , c_hdr_cell_style_decimal),
               ('variance', 1, 0, 'number',  None, variance_formula , c_hdr_cell_style_decimal),
               ('per', 1, 0, 'number', _p.total_per_admin or 0 , None, c_hdr_cell_style_decimal),
        ]
                
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
        
        

general_ledger_xls2('report.account.account_report_general_ledger_xls2',
                   'account.account',
                   parser=general_ledger1)
