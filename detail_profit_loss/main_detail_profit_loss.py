from openerp import models, fields, api
from openerp.osv import fields, osv

class crossovered_budget_lines(osv.osv):
    _inherit = 'crossovered.budget.lines'

    _columns = {
        'account_id': fields.many2one('account.account', 'Account'),
    }
    
class crossovered_budget(osv.osv):
    _inherit = 'crossovered.budget'
    
    _columns = {
        'fiscal_id' : fields.many2one('account.fiscalyear', 'Fiscal Year'),
    }    

class account_account(osv.osv):
    _inherit = 'account.account'
    
    _columns = {
        'detail_profit_loss': fields.selection([('Sales', 'Sales'),('Direct', 'Direct'),('Administrative', 'Administrative')]),
    }
    
class budget_summary_report(osv.osv):
    _name = 'budget.summary.report'
    
    
    def create(self, cr, uid, vals, context=None):
        result=super(budget_summary_report, self).create(cr, uid, vals, context=context)
        acc_acc = self.pool.get('account.account')
        acc_ids = acc_acc.search(cr, uid, [('detail_profit_loss', '=', 'sales')], context=context)
        print "Accounts::::::::::::::::::",acc_ids
        data = []
        final_data = []
        final_data1 = []
        final_data2 = []
        if acc_ids:
            for rec in acc_ids:
                name = acc_acc.browse(cr, uid, rec, context=context).name
                data.append(name)
        print "Sales Account:::::::", data 
         
        budget_obj = self.pool.get('crossovered.budget')
        budget_obj_line = self.pool.get('crossovered.budget.lines')
        acc_move_line = self.pool.get('account.move.line')
        for record in self.browse(cr, uid, [result]):
            print "Fiscal Year::::",record.fiscal_id
            if record.fiscal_id:
                budget_ids = budget_obj.search(cr, uid, [('fiscal_id', '=', record.fiscal_id.id)], context=context)
                print "Budget Id ::::", budget_ids
                line_data = []
                line_data1 = []
                line_data2 = []
                sp_list = []
                sa_list = []
                dp_list = []
                da_list = []
                ap_list = []
                aa_list = []
                vp_list = []
                va_list = []
                vadmin_list = []
                sp = 0.0
                sa = 0.0
                dp = 0.0
                da = 0.0
                ap = 0.0
                aa = 0.0
                vp = 0.0
                va = 0.0
                if budget_ids:
                    for budget in budget_ids:
                        for rec in budget_obj.browse(cr, uid, budget, context=context).crossovered_budget_line:
                            print "coooooooooooo"
                            line_data.append(rec.id)
                        print 'budget line IDdd::::',line_data
                        for rec2 in line_data:
                            print "rec2:::",rec2
                            acc_data =  budget_obj_line.browse(cr, uid, rec2, context=context).account_id
                            print "My Account::::::",acc_data
                            if acc_data.detail_profit_loss == 'Sales':
                                print "Yehhhhhhhh......!!!",acc_data.detail_profit_loss
                                print "Yehhhhhhhh......!!!",acc_data.name
                                plan_amt =  budget_obj_line.browse(cr, uid, rec2, context=context).planned_amount
                                print "Yehhhhhhhh......!!!",plan_amt
                                sp = plan_amt
                                sp_list.append(sp)
                                acc_line = acc_move_line.search(cr, uid, [(('account_id', '=', acc_data.id))], context=context)
                                print "Acc Line :::::::::::::", acc_line
                                total = 0.0
                                variance = 0.0
                                per = 0.0
                                if acc_line:
                                    for new1 in acc_line:
                                        amt = acc_move_line.browse(cr, uid, new1, context=context).debit
                                        total += amt
                                    print "Amount:::::::::", total
                                    sa = total
                                    sa_list.append(sa)
                                    variance =  plan_amt - total
                                    vp = variance
                                    vp_list.append(vp)
                                    print "Variance:::::::::",variance
                                    per = 100 - ((total * 100) / plan_amt)
                                    print "Percent :::::::::", per
                                    final_data.append((0,0,{'account_type':acc_data.detail_profit_loss,
                                                            'account_id':acc_data.id,
                                                            'planned_amount':plan_amt,
                                                            'actual_amount' : total,
                                                            'variance' : variance,
                                                            'percentage' : per,
                                                            }))
                            elif acc_data.detail_profit_loss == 'Direct':
                                print "Yehhhhhhhh......!!!",acc_data.detail_profit_loss
                                print "Yehhhhhhhh......!!!",acc_data.name
                                plan_amt =  budget_obj_line.browse(cr, uid, rec2, context=context).planned_amount
                                print "Yehhhhhhhh......!!!",plan_amt
                                dp = plan_amt
                                dp_list.append(dp)
                                acc_line = acc_move_line.search(cr, uid, [(('account_id', '=', acc_data.id))], context=context)
                                print "Acc Line :::::::::::::", acc_line
                                total = 0.0
                                variance = 0.0
                                per = 0.0
                                if acc_line:
                                    for new1 in acc_line:
                                        amt = acc_move_line.browse(cr, uid, new1, context=context).debit
                                        total += amt
                                    print "Amount:::::::::", total
                                    da = total
                                    da_list.append(da)
                                    variance =  plan_amt - total
                                    va = variance
                                    va_list.append(va)
                                    print "Variance:::::::::",variance
                                    per = 100 - ((total * 100) / plan_amt)
                                    print "Percent :::::::::", per
                                    final_data1.append((0,0,{'account_type':acc_data.detail_profit_loss,
                                                            'account_id':acc_data.id,
                                                            'planned_amount':plan_amt,
                                                            'actual_amount' : total,
                                                            'variance' : variance,
                                                            'percentage' : per,
                                                            }))
                            elif acc_data.detail_profit_loss == 'Administrative':
                                print "Yehhhhhhhh......!!!",acc_data.detail_profit_loss
                                print "Yehhhhhhhh......!!!",acc_data.name
                                plan_amt =  budget_obj_line.browse(cr, uid, rec2, context=context).planned_amount
                                print "Yehhhhhhhh......!!!",plan_amt
                                ap = plan_amt
                                ap_list.append(ap)
                                acc_line = acc_move_line.search(cr, uid, [(('account_id', '=', acc_data.id))], context=context)
                                print "Acc Line :::::::::::::", acc_line
                                total = 0.0
                                variance = 0.0
                                per = 0.0
                                if acc_line:
                                    for new1 in acc_line:
                                        amt = acc_move_line.browse(cr, uid, new1, context=context).debit
                                        total += amt
                                    print "Amount:::::::::", total
                                    aa = total
                                    aa_list.append(aa)
                                    variance =  plan_amt - total
                                    vadmin_list.append(variance)
                                    print "Variance:::::::::",variance
                                    per = 100 - ((total * 100) / plan_amt)
                                    print "Percent :::::::::", per
                                    final_data2.append((0,0,{'account_type':acc_data.detail_profit_loss,
                                                            'account_id':acc_data.id,
                                                            'planned_amount':plan_amt,
                                                            'actual_amount' : total,
                                                            'variance' : variance,
                                                            'percentage' : per,
                                                            }))
                                        
                            
            sper = 0.0
            dper = 0.0
            aper = 0.0
            sper = 100 - ((sum(sa_list) * 100) / sum(sp_list))
            dper = 100 - ((sum(da_list) * 100) / sum(dp_list))
            aper = 100 - ((sum(aa_list) * 100) / sum(ap_list))
            for data1 in self.browse(cr, uid, [result]):
                print "Calllllllllllll"
                self.write(cr, uid, data1.id, {'line_ids': final_data, 
                                               'line_ids1': final_data1,
                                               'line_ids2': final_data2,
                                               'sales_plan' : sum(sp_list),
                                                'sales_actual' : sum(sa_list),
                                                'direct_plan' : sum(dp_list),
                                                'direct_actual' : sum(da_list),
                                                'admin_plan' : sum(ap_list),
                                                'admin_actual' : sum(aa_list),
                                                'variance_plan' : sum(vp_list), 'variance_actual' : sum(va_list),
                                                'admin_ttl_per' : sum(vadmin_list),
                                                'sales_plan_ttl_var' : sper,
                                                'direct_plan_ttl_var' : dper,
                                                'admin_plan_ttl_var' : aper,
                                               })
        return result
    
    
    def _sales_account(self, cr, uid, ids, name, args, context=None):
        
        result = dict.fromkeys(ids, False)
        gr_p = 0.0
        gr_a = 0.0
        gr_v = 0.0
        gr_p_per = 0.0
        gr_a_per = 0.0
        gr_v_per = 0.0
        for record in self.browse(cr, uid, ids, context=context):
            print "Calling You...........>!!!!!!",record.sales_actual
            gr_p = record.sales_plan - record.direct_plan
            gr_a = record.sales_actual - record.direct_actual
            gr_v = record.variance_plan - record.variance_actual
            print "Plan :::",gr_p
            print "Plan :::",gr_a
            gr_p_per = (record.direct_plan * 100) / record.sales_plan
            gr_a_per = (record.direct_actual * 100) / record.sales_actual
            gr_v_per = (record.variance_actual * 100) / record.variance_plan
        result[record.id] = {'gross_profit_plan' : gr_p, 'gross_profit_actual' : gr_a, 'gross_profit_plan_per' : gr_p_per, 'gross_profit_actual_per' : gr_a_per,
                             'gross_profit_variance' : gr_v, 'gross_profit_variiance_per' : gr_v_per}    
        return result 
    
    
    _columns = {
        'company_id': fields.many2one('res.company', 'Company'),
        'fiscal_id' : fields.many2one('account.fiscalyear', 'Fiscal Year'),
#         'account_id' : fields.function(_sales_account, type='many2one', relation='account.account', string='Account'),
        'account_id' : fields.float('Account'),
        'line_ids' : fields.one2many('budget.summary.report.line', 'budget_summary_id', 'Sales'),
        'line_ids1' : fields.one2many('budget.summary.report.line1', 'budget_summary_id1', 'Direct'),
        'line_ids2' : fields.one2many('budget.summary.report.line2', 'budget_summary_id2', 'Administrative'),
        'sales_plan' : fields.float('Sales Plan'),
        'sales_actual' : fields.float('Sales Actual'),
        'direct_plan' : fields.float('Direct Plan'),
        'direct_actual' : fields.float('Direct Actual'),
        'admin_plan' : fields.float('Admin Plan'),
        'admin_actual' : fields.float('Admin Actual'),
        'variance_plan' : fields.float('variance Plan'),
        'variance_actual' : fields.float('variance Actual'),
        'gross_profit_plan' : fields.function(_sales_account, string='GPP', multi='new_call'),
        'gross_profit_actual' : fields.function(_sales_account, string='GPA', multi='new_call'),
        'gross_profit_variance' : fields.function(_sales_account, string='GPV', multi='new_call'),
        'gross_profit_variiance_per' : fields.function(_sales_account, string='GPVP', multi='new_call'),
        'gross_profit_plan_per' : fields.function(_sales_account, string='GPPP', multi='new_call'),
        'gross_profit_actual_per' : fields.function(_sales_account, string='GPAP', multi='new_call'),
        'sales_plan_ttl_var' : fields.float('sale plan variance'),
        'direct_plan_ttl_var' : fields.float('direct plan variance'),
        'admin_plan_ttl_var' : fields.float('admin plan variance'),
        'admin_ttl_per' : fields.float('admiin total percent')
        
        
    }
    _rec_name = 'fiscal_id' 
     
class budget_summary_reprt_line(osv.osv):
    _name = 'budget.summary.report.line'
    
    
    _columns = {
        'budget_summary_id' : fields.many2one('budget.summary.report', 'Budget Summary'),
        'account_type': fields.char('Account Type'),
        'account_id' : fields.many2one('account.account', 'Fiscal Year'),
        'planned_amount' : fields.float('Planned Amount' ,digits=(16,2)),
        'actual_amount' : fields.float('Actual Amount',digits=(16,2)),
        'variance' : fields.float('Variance',digits=(16,2)),
        'percentage' : fields.float('%',digits=(16,2)),
        
    }
    
    
class budget_summary_reprt_line1(osv.osv):
    _name = 'budget.summary.report.line1'
    
    
    _columns = {
        'budget_summary_id1' : fields.many2one('budget.summary.report', 'Budget Summary'),
        'account_type': fields.char('Account Type'),
        'account_id' : fields.many2one('account.account', 'Fiscal Year'),
        'planned_amount' : fields.float('Planned Amount' ,digits=(16,2)),
        'actual_amount' : fields.float('Actual Amount',digits=(16,2)),
        'variance' : fields.float('Variance',digits=(16,2)),
        'percentage' : fields.float('%',digits=(16,2)),
        
    }    
     
class budget_summary_reprt_line2(osv.osv):
    _name = 'budget.summary.report.line2'
    
    
    _columns = {
        'budget_summary_id2' : fields.many2one('budget.summary.report', 'Budget Summary'),
        'account_type': fields.char('Account Type'),
        'account_id' : fields.many2one('account.account', 'Fiscal Year'),
        'planned_amount' : fields.float('Planned Amount' ,digits=(16,2)),
        'actual_amount' : fields.float('Actual Amount',digits=(16,2)),
        'variance' : fields.float('Variance',digits=(16,2)),
        'percentage' : fields.float('%',digits=(16,2)),
        
    }     
                
